<?php

use Illuminate\Database\Seeder;

class RssTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
       	$rss=\App\Rss::insert([
			[
				'name' => 'El Comercio',
				'url' => 'http://www.elcomercio.com/rss',
			],
			[
				'name' => 'El Pais - America',
				'url' => 'http://ep00.epimg.net/rss/elpais/portada_america.xml',
			],
			[
				'name' => 'NY Times - World',
				'url' => 'http://rss.nytimes.com/services/xml/rss/nyt/World.xml',
			],
			[
				'name' => 'La Croix - Universe',
				'url' => 'https://www.la-croix.com/RSS/UNIVERS_WMON',
			],
			[
				'name' => 'Corriere - Politica',
				'url' => 'http://xml2.corriereobjects.it/rss/politica.xml',
			],
			[
				'name' => 'Sueddeutsche - Topthemen',
				'url' => 'http://rss.sueddeutsche.de/rss/Topthemen',
			],
			[
				'name' => 'Globo - EpocaNegocios',
				'url' => 'https://epocanegocios.globo.com/rss/ultimas/feed.xml',
			],
			[
				'name' => 'BBC - World',
				'url' => 'http://feeds.bbci.co.uk/news/world/rss.xml',
			],
			[
				'name' => 'Reuters - Business',
				'url' => 'http://feeds.reuters.com/reuters/businessNews',
			],
			[
				'name' => 'Reuters - Politics',
				'url' => 'http://feeds.reuters.com/Reuters/PoliticsNews',
			],
			[
				'name' => 'IEEE Spectrum - Automaton',
				'url' => 'https://spectrum.ieee.org/rss/blog/automaton/fulltext',
			],
		]);
    }
}
