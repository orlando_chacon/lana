<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeonamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geonames', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->text('name1');
            $table->text('name2');
            $table->text('name3');
            $table->decimal('lat',11,8);
            $table->decimal('lng',11,8);
            $table->bigInteger('population');

            $table->index('lat');
            $table->index('lng');
            $table->index('population');

            $table->index([DB::raw('name1(100)')]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geonames');
    }
}
