<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeonameWord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geoname_word', function (Blueprint $table) {
            $table->unsignedInteger('geoname_id');
            $table->string('word_word');

            $table->foreign('geoname_id')->references('id')->on('geonames');
            $table->foreign('word_word')->references('word')->on('words');

            $table->unique( array('geoname_id','word_word') );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
