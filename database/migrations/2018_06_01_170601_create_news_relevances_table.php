<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsRelevancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_relevances', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('news_id');
            $table->unsignedInteger('geoname_id');
            $table->decimal('lat',11,8);
            $table->decimal('lng',11,8);
            $table->decimal('verticalExtent',11,8);
            $table->decimal('horizontalExtent',11,8);

            $table->index(['news_id', 'geoname_id']);

            $table->index('lat');
            $table->index('lng');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_relevances');
    }
}
