<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>UPM | Location-Aware News Agregator</title>

    @include('common.head')
    @yield('css')
  </head>

  <body id="page-top">
    @include('common.nav')



    @yield('content');
    
    @include('common.footer') 


    @include('common.foot') 
    @yield('js')
  </body>

</html>
