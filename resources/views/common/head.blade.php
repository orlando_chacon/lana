<!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/css/monserrat400700.css" rel="stylesheet" type="text/css">
    <link href='/css/kaushan.css' rel='stylesheet' type='text/css'>
    <link href='/css/droid400700italic.css' rel='stylesheet' type='text/css'>
    <link href='/css/robotoslab.css' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="/css/agency.min.css" rel="stylesheet">