    <div class="portfolio-modal modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body" id="modal-body">
                  <div class="row">
                    <div class="col-md-4">
                      foto
                      Una presunta organización delictiva dedicada al asalto de cooperativas de ahorro y crédito, estaciones de gasolina y tiendas en donde prestan servicios bancarios, fue desarticulada la noche del martes 17 de abril del 2018, en Quito.
                    </div>
                    <div class="col-md-8">
                      <h1>Title</h1>
                      <p>Una presunta organización delictiva dedicada al asalto de cooperativas de ahorro y crédito, estaciones de gasolina y tiendas en donde prestan servicios bancarios, fue desarticulada la noche del martes 17 de abril del 2018, en Quito.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>