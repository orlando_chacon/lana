@extends('layouts.master')

@section('content')

<div id="portfolio">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">News <smaLL>that match your location preferences.</smaLL></h2>
			</div>
		</div>
		<div class="row">
			@foreach ($news as $item)
			<div class="col-sm-4 row">
				<div class="col-sm-12 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-rss-square fa-3x"></i>
						</div>
					</div>
					<div class="container">
						@if ($enclosure = $item->get_enclosure())
						<img class="img-fluid" src="{{ $enclosure->get_link() }}" alt="">
                        @else
						<img class="img-fluid" src="/img/portfolio/01-thumbnail.jpg" alt="">
						@endif
					</div>
				</a>
				<div class="col-sm-12">
					<a href="{{ route('get_item').'?url='. urlencode($item->get_permalink()) }}"><h5>{{ $item->get_title() }}</h5></a>
					<h6>Detected Language: <span class="label label-success">{{$language->detect($item->get_title().' '.$item->get_description())->language}}</span></h6>
					<p class="text-muted">{{ $item->get_description() }}</p>
					
					<h6>Locations detected: <span class="label label-success">
						<?php dd($item->relevances();?>
						@foreach($item->relevances as $location)
							{{$location->lat}}, 
							{{echo $location->lng}}
						@endforeach
					</span></h6>
						<p><small>Posted on {{ $item->get_date('j F Y | g:i a') }}</small></p>
				</div>
				</div>

			</div>
			@endforeach
		</div>
	</div>
</div>
@include('common.modal')

@endsection