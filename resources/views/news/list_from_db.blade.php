@extends('layouts.master')

@section('content')

<div id="portfolio">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">News <smaLL>that match your location preferences.</smaLL></h2>
			</div>
		</div>
		<div class="row">
			@foreach ($articles as $article)
			<div class="col-sm-4 row">
				<div class="col-sm-12 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-rss-square fa-3x"></i>
						</div>
					</div>
					<div class="container">
						<img class="img-fluid" src="{{ $article->image_url}}" alt="">
					</div>
				</a>
				<div class="col-sm-12">
					<a href="{{ $article->url }}"><h5>{{ $article->title }}</h5></a>
					<h6>Detected Language: <span class="label label-success">{{$article->language->language}}</span></h6>
					<p class="text-muted">{{ substr($article->description,0,255) }} ...</p>
					
					<h6>Locations detected: <span class="label label-success">
						@foreach($article->relevances as $location)
							{{$location->geoname->name2}} {{$location->geoname->lat}},{{$location->geoname->lng}}<br>
						@endforeach
					</span></h6>
					<p><small>Posted on {{ $article->post_date }}</small></p>
				</div>
				</div>

			</div>
			@endforeach
		</div>
	</div>
</div>
@include('common.modal')

@endsection