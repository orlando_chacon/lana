<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/nlp', function () {

	$text = "A Russia scholar, she delved into Vladimir V. Putin’s circle of friends in 1990s St. Petersburg and found the blueprint for a “kleptocracy.";

	$rtok = new RegexTokenizer(array(
		array('/\s+/',' '),              
		array('/(\b[a-zA-Z]{4,})[,]{1}/','${1}'),              
		array('/(\b[a-zA-Z]{4,})[.]{1}/','${1}'),              
		array('/\b[a-z]{1,3}\b/', ''), 
		"/ /")
	);
	$tokensArray = $rtok->tokenize($text);
	print_r($rtok->tokenize($s));
});

Route::get('/pdf', function () {
	$g = new \App\Geoname;
	$g->getLocationPatterns();
});

Route::get('/get_locations', function (Request $request) {
	$start=microtime(true);
	$sentence=$request->input('sentence');
	$geoname = new \App\Geoname;
	$locations = $geoname->getLocations($sentence);
	$end=microtime(true);
	// dd($locations);
	foreach ($locations as $location) {
		echo $location->name1.':';
		echo $location->population.'<br>';
	}
	$exeTime = $end - $start;
	echo "Execution Time: $exeTime";

});

Route::get('/initialize', function () {
	$stopWord = new \App\StopWord;
	$geoname = new \App\Geoname;
	$training = new \App\Training;
	$threshold=0.5;

	$stopWord->generate('en',$threshold);echo "\n\n";
	$stopWord->generate('es',$threshold);echo "\n\n";
	$stopWord->generate('fr',$threshold);echo "\n\n";
	$stopWord->generate('de',$threshold);echo "\n\n";
	$stopWord->generate('it',$threshold);echo "\n\n";
	$stopWord->generate('pt',$threshold);echo "\n\n";

	$geoname->getCommonWords($threshold);echo "\n\n";

	$training->getLocationPatterns();
});

Route::get('/training/sentence_patterns', function () {
	$training = new \App\Training;
	$training->getSentencePatterns();
});

Route::get('/training/fetch', function (Request $request) {
	/*

	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=Spain&locations[]=Germany&locations[]=France&locations[]=Italy

	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=Tokyo&locations[]=Hong Kong&locations[]=Paris&locations[]=New York

	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=Montreal&locations[]=Boston&locations[]=Paris&locations[]=Mexico City

	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=New York&locations[]=Atlanta&locations[]=Miami&locations[]=Boston


	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=Quito&locations[]=Cuenca&locations[]=Guayaquil&locations[]=Ambato
	
	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=Madrid&locations[]=Barcelona&locations[]=Sevilla&locations[]=Murcia
	
	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=Bogot%C3%A1&locations[]=Lima&locations[]=Quito&locations[]=Caracas
	
	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=Düsseldorf&locations[]=Köln&locations[]=Berlin&locations[]=München

	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=São Paulo&locations[]=Brasilia&locations[]=Belo Horizonte&locations[]=Brasil


	http://127.0.0.1:8000/training/fetch?narticles=400&start=0&locations[]=Roma&locations[]=Milano&locations[]=Napoli&locations[]=Torino

	*/
	$nArticles=$request->input('narticles');
	$start=$request->input('start');
	$locations=$request->input('locations');

	$trainingArticle= new \App\TrainingArticle;
	do
	{
		if($nArticles>100){
			$nArticles-=100;
			$start+=100;
			$tmpN=100;
		}else{
			$tmpN=$nArticles;
			$nArticles=0;
		}
		$links = $trainingArticle->fetch($locations, $tmpN,$start);
	}while($nArticles>0);
	// $links = $training->newsOfLocationsLinks(["Tokyo", "Hong Kong", "Paris", "New York"],$nArticles,$start);
});
Route::get('/training/prepare', function () {
	$trainingArticles= \App\TrainingArticle::all();
	foreach ($trainingArticles as $trainingArticle) {
		# code...
		$trainingArticle->splitInSentences();
	}
	$trainingArticle->getSurroundingWords();
});
Route::get('/training_articles/build_model', function (Request $request) {
	$nleft=$request->input('nleft');
	$nright=$request->input('nright');

	$trainingArticle= new \App\TrainingArticle;
	$links = $trainingArticle->buildNMGramModel($nleft,$nright);
});

Route::get('/ngram', function (Request $request) {
	header('Content-Type: text/html; charset=UTF-8');
	$corpus=$request->input('corpus');
	$n = $request->input('n');

	if (!isset($corpus,$n)) {
		echo "You must specify the corpus and the n!";
		exit;
	}
	$g= new \App\Geoname;
	$g->nGramParser($corpus,$n);
});

Route::get('/test', function (Request $request) {
	$n=$request->input('n');
	$rw = new \App\RelevantWord;
	echo $rw->fetch($n);
});




Route::get('/detect_language', function () {

	$corpus='This program displays three statistics for each text: average word length, average sentence length, and the number of times each vocabulary item appears in the text on average (our lexical diversity score). Observe that average word length appears to be a general property of English, since it has a recurrent value of 4. (In fact, the average word length is really 3 not 4, since the num_chars variable counts space characters.) By contrast average sentence length and lexical diversity appear to be characteristics of particular authors.';

	$corpus="El color es la impresión producida por un tono de luz en los órganos visuales, o más exactamente, es una percepción visual que se genera en el cerebro de los humanos y otros animales al interpretar las señales nerviosas que le envían los fotorreceptores en la retina del ojo, que a su vez interpretan y distinguen las distintas longitudes de onda que captan de la parte visible del espectro electromagnético.

Todo cuerpo iluminado absorbe una parte de las ondas electromagnéticas y refleja las restantes. Las ondas reflejadas son captadas por el ojo e interpretadas en el cerebro como distintos colores según las longitudes de ondas correspondientes.

El ojo humano sólo percibe las longitudes de onda cuando la iluminación es abundante. Con poca luz se ve en blanco y negro. En la superposición de colores luz (denominada síntesis aditiva de color), el color blanco resulta de la superposición de todos los colores, mientras que el negro es la ausencia de luz. En la mezcla de pigmentos (denominada síntesis sustractiva de color), trátese de pinturas, tintes, tintas o colorantes naturales para crear colores, el blanco solo se da si el pigmento o el soporte son de ese color, reflejando toda la luz blanca, mientras que el negro es resultado de la superposición completa de los colores cian, magenta y amarillo, una mezcla que en cierta medida logra absorber todas las longitudes de onda de la luz.

La luz blanca puede ser descompuesta en todos los colores del espectro visible por medio de un prisma (dispersión refractiva). En la naturaleza esta descomposición da lugar al arco iris.";

	$corpus="Au terme de leur rencontre avec le pape François sur leur gestion calamiteuse des abus sexuels, les 32 évêques chiliens ont annoncé, vendredi 18 mai, remettre tous leur charge pastorale entre les mains du pape. Dans une déclaration lue à la presse au terme des trois jours de rencontres qu’ils ont eues au Vatican, les évêques chiliens ont expliqué avoir longuement réfléchi aux interpellations du pape lors de leur première rencontre avec ce dernier, mardi matin.

Ainsi a mûri l’idée que, pour être en meilleure syntonie avec la volonté du Saint-Père, il convenait de déclarer notre absolue disponibilité pour remettre nos charges pastorales entre les mains du pape », ont déclaré Mgr Juan Ignacio Gonzalez, évêque de San Bernardo, et Mgr Fernando Ramos, évêque auxiliaire de Santiago.";

	$corpus="RIO — Chegando ao seu oitavo dia, a greve dos caminhoneiros já afeta serviços pelo país. Além da falta de abastecimento de gasolina em postos em diversos municípios, o bloqueio das rodovias gerou impacto no funcionamento de escolas, universidades, aeroportos, mercados, bancos e até do poder judiciário.";
	$g= new \App\Language;
	var_dump($g->detect($corpus));
});


Route::get('/content', function () {

	$url1 = "http://www.elcomercio.com/deportes/psg-espera-neymar-cerrar-temporada.html";
	$url2 = "http://www.elcomercio.com/actualidad/limpieza-laguna-parque-lacarolina-agua.html";

	// create curl resource 
    $ch = curl_init();

    // set url 
    curl_setopt($ch, CURLOPT_URL, $url1); 

    //return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

    // $output contains the output string 
    $output1 = curl_exec($ch);

    // close curl resource to free up system resources 
    curl_close($ch);

    $ch = curl_init();
    // set url 
    curl_setopt($ch, CURLOPT_URL, $url2);

    //return the transfer as a string 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

    // $output contains the output string 
    $output2 = curl_exec($ch);

    // close curl resource to free up system resources 
    curl_close($ch);






	$rtok = new RegexTokenizer(array(
		array('/\s+/',' '),              
		array('/[\s\S]*?(<[\s]*body[\s\S]*?>[\s\S]*?<[\/\s]*body[\s\S]*?>)[\s\S]*?/','${1}'),              
		
		
		// array('/(<[^>]*>)/', '${1}${1}'),
		"/\n/")
	);
	$tokensArray = $rtok->tokenize($output1);
	echo "\n";
	print_r($rtok->tokenize($output1));

});


Route::get('/get_item', 'NewsController@getWebItem')->name('get_item');

Route::get('/rss/fetch', function () {
    	header('Content-Type: text/html; charset=utf-8');
	
	$start=microtime(true);
	
	$rsses=\App\Rss::all();
	$feedsUrls=[];
	foreach ($rsses as $rss) {
		// array_push($feedsUrls, $rss->url);
		$rss->fetch();
	}
	// $rss=new \App\Rss;

	$end=microtime(true);
	$exeTime = $end - $start;
	echo "Execution Time: $exeTime";

});

Route::get('/geonames/seed', function () {
	$geoname=new \App\Geoname;
	$geoname->seed();
});

Route::get('/geonames/tokenize', function () {
	$geoname=new \App\Geoname;
	$geoname->tokenize();
});

Route::get('/geonames/import_sql', function () {
	$geoname=new \App\Geoname;
	$geoname->importSql();
});

Route::get('/geonames/split_words', 'GeonameController@splitWords');


Route::get('/user/profile', 'UserController@profile')->name('user_profile');
Route::get('/news', 'NewsController@index')->name('news');
Route::get('/news/location', 'NewsController@location')->name('news_location');
Route::get('/news/{article}', 'NewsController@show')->name('news_show');


function getWikipediaPage($page) {
    ini_set('user_agent', 'NlpToolsTest/1.0 (tests@php-nlp-tools.com)');
    $page = json_decode(file_get_contents("http://en.wikipedia.org/w/api.php?format=json&action=parse&page=".urlencode($page)),true);
    return preg_replace('/\s+/',' ',strip_tags($page['parse']['text']['*']));
}