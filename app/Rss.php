<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rss extends Model
{
    public function news()
	{
		return $this->hasMany('App\News');
	}

	public function fetch($feedsUrls=NULL)
	{
    	ini_set('max_execution_time', 36000);

		$start=microtime(true);

		mb_internal_encoding('utf-8');
		$feed = new \SimplePie();
		$feed->set_cache_location(storage_path());
		if (isset($feedsUrls)) {
			$feed->set_feed_url($feedsUrls);
		}else{
			$feed->set_feed_url([$this->url]);
		}
		$feed->init();
		$feed->handle_content_type();

		$lang=new \App\Language;
		$htmlParser = new \App\HtmlParser;
		$items=$feed->get_items();


		$end=microtime(true);
		$exeTime = $end - $start;
		echo "\nFetching Feeds: $exeTime sec\n";


		$start=microtime(true);

		$curlMulti = curl_multi_init();
	    $corpora="";
	    $chs=[];
	    $urls=[];

		foreach ($items as $item) {
			try {
				$article = new \App\News;
				if ($enclosure = $item->get_enclosure()){
					$article->image_url = $enclosure->get_link();
				}else{
					$article->image_url = '/img/portfolio/01-thumbnail.jpg';
				}
				$article->title = $item->get_title();

				
				$article->rss_id = $this->id;
				$article->language_id = $lang->detect($item->get_title().' '.$item->get_description())->id;
				$article->description = $htmlParser->clean($item->get_description(),false,false);
				// $article->description = $item->get_description()!==NULL ? $item->get_description():'';
				$article->url = $item->get_permalink();
				$article->post_date = $item->get_date('Y-m-d H:i:s');
				$article->save();



				//Curl preparation
				$chs[$article->id] = curl_init();
				// echo $article->url;
		    	curl_setopt($chs[$article->id], CURLOPT_URL, $article->url); 

			    //return the transfer as a string 
			    curl_setopt($chs[$article->id], CURLOPT_RETURNTRANSFER, 1);
			    curl_setopt($chs[$article->id], CURLOPT_CONNECTTIMEOUT, 20); 
				curl_setopt($chs[$article->id], CURLOPT_TIMEOUT, 120);
			    curl_setopt($chs[$article->id], CURLOPT_FOLLOWLOCATION, true);
			    curl_setopt($chs[$article->id], CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

				curl_multi_add_handle($curlMulti,$chs[$article->id]);

			} catch (\Exception $e) {
				
			}
		}
		$end=microtime(true);
		$exeTime = $end - $start;
		echo "\nSaving Feeds: $exeTime sec\n";

		$start=microtime(true);

		$running = null;
		do{
			curl_multi_exec($curlMulti, $running);
		}while($running > 0);


		$end=microtime(true);
		$exeTime = $end - $start;
		echo "\nFetching Full Articles: $exeTime sec\n";

		$start=microtime(true);

		foreach($chs as $id => $ch) {
			try {
				
				$result[$id] = curl_multi_getcontent($ch);
				$article=\App\News::find($id);
				$article->corpus = $htmlParser->clean($result[$id],false,true);
				$article->language_id = $lang->detect($article->corpus)->id;
				$article->save();
				$article->detectLocations();
			} catch (\Exception $e) {
				echo $result[$id];
				exit;
			}
		}

		$end=microtime(true);
		$exeTime = $end - $start;
		echo "\nParsing Full Articles: $exeTime sec\n";

	}
}
