<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use \NlpTools\Tokenizers\WhitespaceTokenizer;
use \NlpTools\Similarity\CosineSimilarity;	

use \NlpTools\Tokenizers\WhitespaceAndPunctuationTokenizer;
use NlpTools\Tokenizers\RegexTokenizer;

use \App\StopWord;
use \App\Language;

class Geoname extends Model
{
	public $timestamps = false;

    public function words()
    {
        return $this->belongsToMany('App\Word');
    }
    public function relevances()
    {
        return $this->hasMany('App\Word');
    }

    public function seed($fileName='allCountries.txt'){
    	ini_set('max_execution_time', 36000);
    	$filename=public_path().'/resources/geonames/'.$fileName;
    	try
		{
		    $lines = [];
		    $handle = fopen($filename, "r");
		    $data=[];
		    $i=0;
		    while(!feof($handle)) {
		    	$i++;
		        $line = trim(fgets($handle));
		        $line=substr($line, 0,-1);
		    	$xline=explode("\t", $line);
		    	if(isset($xline[0], $xline[1], $xline[2], $xline[3],$xline[4], $xline[5], $xline[14]))
		    		if ($xline[14]) 
		    			array_push($data, ['code'=>$xline[0], 'name1'=>$xline[1], 'name2'=>$xline[2], 'name3'=> $xline[3],'lat'=>$xline[4], 'lng'=>$xline[5], 'population'=>$xline[14] ]);
            	if($i%5000==0){
            		\App\Geoname::insert($data);
            		$data=[];
            	}
		    	echo "\n";
		    }
		    \App\Geoname::insert($data);
		}
		catch (Illuminate\Filesystem\FileNotFoundException $exception)
		{
		    die("The 'allCountries.txt' file doesn't exist.");
		}
    }

    public function tokenize()
    {
    	ini_set('max_execution_time', 360000);
    	$geonames=\App\Geoname::where('id','>',301844)->get();//482638 //336745 //301845
    	$lang=new \App\Language;
    	foreach ($geonames as $geoname) {
    		$words='';
    		$geoname->name2 = preg_replace($lang->getLanguageRexExp('en'), ' ', $geoname->name2);
    		$geonameWords=explode(' ', $geoname->name2);
    		// $geonameWords=explode(' ', $geoname->name1);
    		foreach ($geonameWords as $geonameWord) {
    			try {
    				$word=new \App\Word;
	    			$word->word=mb_strtolower($geonameWord);
	    			$word->save();
    			} catch (\Exception $e) {
    				$word=\App\Word::where('word', mb_strtolower($geonameWord))->first();
    			}
    			try {
	    			$geoname->words()->attach($word->word);
    			} catch (\Exception $e) {
    				
    			}
    		}
    	}
    }

    public function getLocations($search){
		// $search = preg_replace(['/\b[a-z]\w+/','/\s+/'], ' ', $search);

		$search = trim(preg_replace(['/\b\p{Ll}*\b/u','/\s+/'], ' ', $search));
    	$searchAux=$search;//Auxiliar varible to store the query
		// echo "Search: " . $search . "\n";
		
		// echo "-----$search-----\n";

		if ($search!='') {
			
		
			$search=explode(' ', trim($search) );

			usort($search,array($this,'orderByLength'));
			
			if(count($search)==1){
		    	$commonLocationWord = new \App\CommonLocationWord;
		    	if($commonLocationWord->exist($search[0])){
					$geonames=\App\Geoname::where('name1',$search[0])->where('population','>',10000)->orderBy('population', 'desc')->get();
				}else{
					$relevantWord=\App\RelevantWord::where('word_word',$search[0])->first();
					if(isset($relevantWord)){
						return $relevantWord->word->geonames;
					}
					else {
						$geonames=\App\Geoname::where('id','FALSE ID')->get();
						return $geonames;
					}
					// return $relevantWords->Word;
				}
			}

			foreach ($search as $singleWord) {
				if (!isset($previosResult)) {
					$geonames=\DB::table('geoname_word')
								->join('geonames','geoname_word.geoname_id','=','geonames.id')
								->where('geoname_word.word_word',$singleWord)
								->where('geonames.population','>',10000)
								->get();
				}else{
					$geonames=\DB::table('geoname_word')
							->where('geoname_word.word_word',$singleWord)
							->where('geonames.population','>',10000)
							->whereIn('geoname_word.geoname_id',$previosResult)
							->join('geonames','geoname_word.geoname_id','=','geonames.id')
							->get();
				}
				$previosResult=[];
				foreach ($geonames as $geoname) {
					array_push($previosResult, $geoname->geoname_id);
				}
			}

			$geonames=\App\Geoname::whereIn('id',$previosResult)->orderBy('population', 'desc')->get();
			foreach ($geonames as $key=>&$geoname) {
				if (strlen($searchAux) < strlen($geoname->name2)*0.5) {
					unset($geonames[$key]);
				}
			}
			
			
			return $geonames;
		}else{
			$geonames=\App\Geoname::where('id','FALSE ID')->get();
			return $geonames;
		}
    }

	private function orderByLength($a, $b){
		return strlen($b) - strlen($a);
	}

	

	


	public function orderPlaces($a, $b){
		if ($a[0].$a[2]>$b[0].$b[2]) {
			return 1;
		}elseif($a[0].$a[2]<$b[0].$b[2]){
			return -1;
		}else{
			return 0;
		}
	}

	
	
	public function getCommonWords($threshold=0.40){
        ini_set('max_execution_time', 36000);
		$totalWordsQuery="select count(gw.word_word) as total 
		from geoname_word gw";

		$totalWords = \DB::select($totalWordsQuery)[0]->total;

		$groupedWordsQuery="select gw.word_word as word, count(gw.word_word) as total 
		from geoname_word gw
		group by gw.word_word
        order by count(gw.word_word) desc";

		$groupedWords = \DB::select($groupedWordsQuery);

		$thresholdSum=0;
	    $nTokens=0;

	    foreach ($groupedWords as $groupedWord) {
	    	$probability = $groupedWord->total/$totalWords;
	    	$thresholdSum+=$probability;
	    	if($thresholdSum<$threshold){
	    		$printProbability = number_format($probability,2);
	    		echo $groupedWord->word." : $printProbability : ".$groupedWord->total." | ";
	    		$nTokens++;
	    		// echo $nTokens % 4 == 0 ? " \\\\\n" : " & ";
	    		$commonLocationWord = new \App\CommonLocationWord;
	    		$commonLocationWord->word_word = $groupedWord->word;
	    		$commonLocationWord->probability = $probability;
	    		$commonLocationWord->save();
	    	}else{
	    		print "\n\nTotal stopwords: $nTokens; threshold=$thresholdSum";
	    		return false;
	    	}
	    }
	}

	

	public function newsOfLocationsLinksSingle($cities, $nArticles=10){
		if (is_array($cities)) {
			foreach ($cities as &$city) {
				$city="\"$city\"";
				$city=str_replace(' ', '+', $city);
			}
			$query=implode("+and+", $cities);
		}
		$searchUrl = "https://www.google.com.ec/search?tbm=nws&q=" . $query . '&num='.$nArticles;
		echo "$searchUrl\n\n";
		$ch = curl_init();

	    // set url 
	    curl_setopt($ch, CURLOPT_URL, $searchUrl); 

	    //return the transfer as a string 
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

	    // $output contains the output string 
	    $output = curl_exec($ch);
	    // close curl resource to free up system resources 
	    curl_close($ch);

	    preg_match_all('/<\s*h3[^>]*>.*?href="\/url\?q=.*?(.*?)\&.*?<\s*\/h3\.*>/', $output, $matches,PREG_OFFSET_CAPTURE);

	    $corpora="";
	    foreach ($matches[1] as $newsUrl) {
		    $ch = curl_init();
		    $newsUrl[0];
		    // set url 
		    curl_setopt($ch, CURLOPT_URL, $newsUrl[0]); 

		    //return the transfer as a string 
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

		    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

		    // $output contains the output string 
		    $output = curl_exec($ch);
		    // close curl resource to free up system resources 
		    curl_close($ch);

		    //Remove <script>,<style> and <nav> tags and its content
		    $output=preg_replace(['/<script[^>]*>.*?<\/script>/s', '/<style[^>]*>.*?<\/style>/s','/<nav[^>]*>.*?<\/nav>/s','/<ul[^>]*>.*?<\/ul>/s','/<ol[^>]*>.*?<\/ol>/s'], ' ', $output);

		    //Get paragraph content
		    /*preg_match_all('/<p\b[^>]*?>(.*?)<\s*\/*p\s*?>/s', $output, $matchesP,PREG_OFFSET_CAPTURE);*/
		    preg_match_all('/<p\b[^>]*?>(.*?)<\s*\/*p\s*?>/s', $output, $matchesP,PREG_OFFSET_CAPTURE);
		    $corpus='';

		    foreach ($matchesP[1] as $matchP) {
		    	$corpus .= preg_replace('/<[^>]*?>/s',' ',' '.$matchP[0]);
				$corpus = html_entity_decode($corpus);
		    }

		    //Create and store into the DB the fetched training article
		    try {
		    	$training_article= new \App\TrainingArticle;
			    $training_article->url=$newsUrl[0];
			    $training_article->article=$corpus;
			    $training_article->query=$query;
			    $training_article->save();
			    $corpora.="\n*********************************\n".$newsUrl[0]."\n***\n".$corpus;
		    } catch (\Exception $e) {
		    	echo "\n\n*********************************\nThe article already exists in the DB\n*********************************\n\n\n";
		    }
		   
	    }

	    echo $corpora;

	}
	public function importSql(){
    	ini_set('max_execution_time', 36000);
		
		$dbhost=env('DB_HOST', false);
    	$dbport=env('DB_PORT', false);

    	$dbname=env('DB_DATABASE', false);
    	$dbuser=env('DB_USERNAME', false);
    	$dbpass=env('DB_PASSWORD', false);
    	echo

    	$filename=public_path().'/resources/sql/geonames.sql';

    
    	$query="mysql -h $dbhost -u $dbuser --password=$dbpass $dbname < $filename";
    	echo $query;
    	system($query);
	}
	
	

	private function orderPrevNext($a, $b){
		if ($a[0].$a[1]>$b[0].$b[1]) {
			return 1;
		}elseif($a[0].$a[1]<$b[0].$b[1]){
			return -1;
		}else{
			return 0;
		}
	}

	private function sortBoW($a, $b){
		return $b-$a;
	}

}
