<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HtmlParser extends Model
{
    public function clean($rawHTML, $onlyExtractBody=false,$onlyP=true){
    	$lang=new \App\Language;
		$encoding=mb_detect_encoding($rawHTML);
    	//Remove <script>,<style> and <nav> tags and its content
	    $rawHTML=preg_replace(['/<script[^>]*>.*?<\/script>/s', '/<style[^>]*>.*?<\/style>/s','/<nav[^>]*>.*?<\/nav>/s','/<ul[^>]*>.*?<\/ul>/s','/<ol[^>]*>.*?<\/ol>/s'], ' ', $rawHTML);

	    //Get paragraph content
	    if ($onlyP) {
		    preg_match_all('/<p\b[^>]*?>(.*?)<\s*\/*p\s*?>/s', $rawHTML, $paragraphs,PREG_OFFSET_CAPTURE);
		    
		    $corpus='';
		    foreach ($paragraphs[1] as $paragraph) {
		    	$corpus .= preg_replace(['/<[^>]*?>/s','/\s+/s'],' ',' '.$paragraph[0]);
		    }
		}else{
			$corpus=$rawHTML;
			$corpus = preg_replace(['/<[^>]*?>/s','/\s+/s'],' ',' '.$corpus);
		}

		$detectedLanguge = $lang->detect($corpus);
	    $corpus=mb_ereg_replace(['/[^\p{L}.,:;!?%]/s','/\s+/'], ' ', $corpus);

		$corpus = html_entity_decode($corpus, ENT_COMPAT | ENT_HTML401, 'UTF-8');
		
		$corpus = trim(html_entity_decode($corpus,ENT_QUOTES | ENT_XML1, 'UTF-8'));
		$corpus = mb_convert_encoding($corpus, "UTF-8", mb_detect_encoding($corpus, "UTF-8, ISO-8859-1, ISO-8859-15", true));

		// echo mb_detect_encoding($corpus). ']\n'.$corpus;
		return $corpus;
    }
}
