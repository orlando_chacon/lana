<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile(){
        $pageTitle=new \stdClass();
        $pageTitle->title='Usuario';
        $pageTitle->subtitle='Perfil de usuario';
        $pageTitle->breadcrumbs=['Usuario'=>route('user_profile')];

    	$user='';
    	return view('users.profile', compact('user','pageTitle'));
    }
}
