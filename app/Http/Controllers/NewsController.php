<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Feeds;
use \App\Geoname;

class NewsController extends Controller
{
    public function index(){
        //Modify to match the desired location
        $articles = \App\News::all();
        
        return view('news.list_from_db', compact('articles'));
    }
    public function location(Request $req){
        //Modify to match the desired location
        $lat = $req->input('lat');
        $lng = $req->input('lng');
        $hex = $req->input('hex');
        $vex = $req->input('vex');
        $articles = \App\News::whereHas('relevances', function($q) use($lat, $lng, $hex, $vex){
            $q->where([
                ['lat','>=',$lat-$hex],
                ['lat','<=',$lat+$hex],
                ['lng','>=',$lng-$vex],
                ['lng','<=',$lng+$vex],

                ]);
        })->get();

        
        return view('news.list_from_db', compact('articles'));
    }


    public function show(\App\News $article){
        return view('news.show', compact('article'));
    }

    public function test(){
    	$feed = new \SimplePie();
		$feed->set_cache_location(storage_path());
		$feed->set_feed_url([//'http://www.elcomercio.com/rss',
                            // 'http://ep00.epimg.net/rss/elpais/portada_america.xml',
							'http://rss.nytimes.com/services/xml/rss/nyt/World.xml',
                            // 'https://www.la-croix.com/RSS/UNIVERS_WMON',
                            // 'http://xml2.corriereobjects.it/rss/politica.xml',
                            // 'http://rss.sueddeutsche.de/rss/Topthemen',
                            // 'https://epocanegocios.globo.com/rss/ultimas/feed.xml'
                            ]);
		$feed->init();
		$feed->handle_content_type();

		$news=$feed->get_items();
        $geoname=new Geoname;
        $language=new \App\Language;
        $article=new \App\News;
    	return view('news.list', compact('news','pageTitle','geoname','language','article'));

    }
    public function getWebItem(Request $req){
    	$url = $req->input('url');
    	$urlDecoded = urldecode($req->input('url'));


    	// create curl resource 
        $ch = curl_init();

        // set url 
        curl_setopt($ch, CURLOPT_URL, $urlDecoded); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

        // $output contains the output string 
        $output = curl_exec($ch);

        // close curl resource to free up system resources 
        curl_close($ch);
        echo $output;
        // echo strip_tags($output);


    	// echo $urlDecoded;
    }
}
