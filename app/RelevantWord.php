<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelevantWord extends Model
{
	public $timestamps = false;
	public function word()
	{
		return $this->belongsTo('App\Word');
	}

    public function fetch($value='')
    {
    	$query = "SELECT gw.word_word, count(gw.word_word) as n
		FROM geonames g, lana.geoname_word gw, lana.words w 
		where gw.word_word=w.word and 
		
		gw.geoname_id = g.id and 
		
		length(w.word)>=4
		group by(gw.word_word)
		having count(gw.word_word)<=10
		order by count(gw.word_word)";

		$relevantWord = \DB::select($query);
		foreach ($relevantWord as $word) {
			$relevantWord=new \App\RelevantWord;
			$relevantWord->word_word=$word->word_word;
			$relevantWord->n=$word->n;
			$relevantWord->save();

			echo $word->word_word.":".$word->n."\n<br>\n";
		}
    }
}
