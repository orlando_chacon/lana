<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StopWord extends Model
{
    public function language()
	{
		return $this->belongsTo('App\Language');
	}

	public function generate($language='en', $threshold=0.5){
        ini_set('memory_limit','200M');
        ini_set('max_execution_time', 36000);
        $lang= new \App\Language;
        $languageRegexp=$lang->getLanguageRexExp($language);
        $files = glob( public_path().'/resources/gutenberg/'.$language.'/*');
        $corpus='';
        foreach ($files as $fileName) {
                
                $corpus .="\n".file_get_contents($fileName);
        }
        
        $corpus=mb_strtolower(preg_replace([$languageRegexp,'/\s+/'], [' ',' '], $corpus));
        $corpus=mb_strtolower(preg_replace([$languageRegexp,'/\s+/'], [' ',' '], $corpus));

        // $corpus=$this->removeStopWords($corpus);

        $tokens=explode(' ', $corpus);

        $nTokens=count($tokens);

        $tokensFequency=[];

        foreach ($tokens as $token) {
            $md5Index=md5($token);
            if (array_key_exists($md5Index, $tokensFequency)){
                $tokensFequency[$md5Index][0]++;
            }else{
                $tokensFequency[$md5Index][0]=1;
                $tokensFequency[$md5Index][1]=0;
                $tokensFequency[$md5Index][2]=$token;
            }
        }
        foreach ($tokensFequency as &$tokenF) {
            $tokenF[1]=$tokenF[0]/$nTokens;
        }

        uasort($tokensFequency,array($this,'orderInverseBySubindex1'));
        $thresholdSum=0;
        $nTokens=0;
        try {
            
            $lang = new \App\Language;
            $lang->language = $language;
            $lang->save();
        } catch (\Exception $e) {
            $lang = \App\Language::where('language',$language)->first();
        }

        foreach ($tokensFequency as $key => $tokenF) {
            try {
                $thresholdSum+=$tokenF[1];
                if($thresholdSum<$threshold){
                    $printProbability = number_format($tokenF[1],6);
                    echo $tokenF[2]." & $printProbability";
                    $nTokens++;
                    echo $nTokens % 4 == 0 ? " \\\\\n" : " & ";
                    $sWord = new \App\StopWord;
                    $sWord->word=$tokenF[2];
                    $sWord->probability=$tokenF[1];
                    $sWord->language_id=$lang->id;
                    $sWord->save();
                }else{
                    print "\n\nTotal stopwords: $nTokens; threshold=$threshold";
                    return false;
                }
            } catch (\Exception $e) {
                print "\n\nError!!";
            }
        }
    }

    public function remove($corpus, $preserveCase=false, $caseSensitive=true, $lowerFirst=true){
        $language=new \App\Language;
        $detectedLanguage=$language->detect($corpus);
        $languageRegexp=$language->getLanguageRexExp($detectedLanguage->language);

        $language = Language::where('language', $detectedLanguage->language)->first();
        

        if ($lowerFirst) {
            $corpus = preg_replace_callback('/(^\b\w*\b)/', function ($word) {
                return mb_strtolower($word[1]);
            }, $corpus);
        }

        // $options='';
        // if ($caseSensitive) {
        //     $options='s';
        // }
        if($preserveCase){
            $corpus=preg_replace([$languageRegexp,'/\s+/'], [' ',' '], $corpus);
        }else{
            $corpus=preg_replace([$languageRegexp.$options,'/\s+/'], [' ',' '], mb_strtolower($corpus));
        }
        // echo $corpus;
        $sWordsRegExp=[];
        foreach ($language->stopWords as $sWord) {
            array_push($sWordsRegExp, '/\b'.$sWord->word.'\b/');
        }
        array_push($sWordsRegExp, '/\s+/');

        $corpus=preg_replace($languageRegexp, ' ' , $corpus);
        $corpus=preg_replace($sWordsRegExp, ' ' , $corpus);

        if (!$preserveCase) {
            $corpus=mb_strtolower($corpus);
        }

        return $corpus;
    }

    private function orderInverseBySubindex1($a, $b){
		if ($a[1]>$b[1]) {
			return -1;
		}elseif($a[1]<$b[1]){
			return 1;
		}else{
			return 0;
		}
	}
}
