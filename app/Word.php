<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
	protected $primaryKey = 'word';
    public $incrementing = false;
    public $timestamps = false;

    public function geonames()
    {
        return $this->belongsToMany('App\Geoname');
    }
    public function relevantWord()
	{
		return $this->hasOne('App\RelevantWord');
	}
}
