<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
	public function getLocationPatterns($language="en"){
	    $directories = glob( public_path().'/resources/gmb-1.1.0/data/*' , GLOB_ONLYDIR);
		$places = [];
		$i=0;
	    $corpus='';
	    //Inspect all annotated data and find surrounding words to places.
	    foreach ($directories as $directorie) {
	    	$subdirectories = glob( $directorie.'\*' , GLOB_ONLYDIR);
	    	foreach ($subdirectories as $subdirectorie) {
	    		# code...
	    		$fileName=$subdirectorie.'\en.tags';
	    		
	    		$corpus .="\n".file_get_contents($fileName);
	    	}
	    }

	    preg_match_all('/(.*?)\t.*?\n(.*Location\n)+(.*?)\t/', $corpus, $matches,PREG_OFFSET_CAPTURE);

	    $n = count($matches[0]);
	    $dynRegExp=[];
	    for ($i=0; $i < $n ; $i++) { 
	    	// echo "{$matches[1][$i][0]}...{$matches[3][$i][0]}\n";
	    	$md5Index=md5($matches[1][$i][0].$matches[3][$i][0]);
	    	if(array_key_exists($md5Index,$dynRegExp)){
	    		$dynRegExp[$md5Index][1]++;
	    	}else{
	    		try {
	    			
		    		$search=[".","(",")"];
		    		$replace=["\\.","\\(","\\)"];
		    		$previous = str_replace($search,$replace,$matches[1][$i][0]);
		    		$next = str_replace($search,$replace,$matches[3][$i][0]);
		    		$dynRegExp[$md5Index][0] = '/'.$previous.'\t.*?\n(.*?\n){1,2}'.$next.'\t/';
		    		$dynRegExp[$md5Index][1]=1;
		    		$dynRegExp[$md5Index][2]=0;
		    		$dynRegExp[$md5Index][3]=0;
	    		} catch (\Exception $e) {
	    			echo "Dynamic regular expresion contruction error!";
	    			return false;
	    		}
	    	}
	    }

	    foreach ($dynRegExp as &$regExp) {
	    	if($regExp[1]>1){
		    	preg_match_all($regExp[0], $corpus, $matches,PREG_OFFSET_CAPTURE);
		    	$regExp[2] = count($matches[0]);
		    	$den=($regExp[2]!=0 ? $regExp[2] : $regExp[1]);
		    	$regExp[3] = $regExp[1]/$den;
	    	}
	    	else{
	    		unset($regExp);
	    	}
	    }
	    uasort($dynRegExp,array($this,'orderByProbability'));

	    foreach ($dynRegExp as &$regExp) {
	    	if ($regExp[0]>5) {
    			echo ($regExp[0])."\t".$regExp[1]."\t".$regExp[2]."\t".$regExp[3]."\n";
	    	}
	    }
	}

	//Get Brown Sentence patterns
	public function getSentencePatterns($language="en"){
	    $files = glob( public_path().'/resources/brown/*');
		
		$corpus='';    
    	foreach ($files as $fileName) {
    		# code...
    		$corpus .="\n".file_get_contents($fileName);
    		break;
    	}
    	preg_match_all('/[^\s]*\/[^\s]*/', $corpus, $matches,PREG_OFFSET_CAPTURE);

    	// print_r($matches);
    	$morphoProbabilities=[];
    	$isFirstWord=true;
    	foreach ($matches[0] as $match) {
    		$nextWordTag=explode("/", $match[0]);

    		//Catch first word


    		$isFirstWord=false;
    		if (isset($wordTag, $nextWordTag)) {
    			if (isset($morphoProbabilities[$wordTag[1]][$nextWordTag[1]])) {
    				$morphoProbabilities[$wordTag[1]][$nextWordTag[1]]++;
    			}else{
    				$morphoProbabilities[$wordTag[1]][$nextWordTag[1]]=1;
    			}
    		}

    		$wordTag=$nextWordTag;
    	}
    	print_r($morphoProbabilities);
	    
	}



	public function getAnotations($fileName=NULL){
  //   	try
		// {
		    $i=0;

		    $directories = glob( public_path().'/resources/gmb-1.1.0/data/*' , GLOB_ONLYDIR);

			$places = [];
			$i=0;
		    
		    //Inspect all annotated data and find surrounding words to places.
		    foreach ($directories as $directorie) {
		    	$subdirectories = glob( $directorie.'\*' , GLOB_ONLYDIR);
		    	foreach ($subdirectories as $subdirectorie) {
		    		# code...
		    		$fileName=$subdirectorie.'/en.tags';
		    		
		    		if (!isset($fileName)) {
						echo "Error!!";
					}

				    $handle = fopen($fileName, "r");
				    $prev='';
				    $next='';

				    $found=false;
				    while(!feof($handle)) {
				    	$line = trim(fgets($handle));

				    	if($found){
				    		$places[$i][0]=preg_replace('/^(.*?)\t.*/', '${1}', $prev);
				    		$places[$i][1]=preg_replace('/(.*?)\t.*/', '${1}', $line);
				    		$places[$i][2]=$fileName;
				    		$found=false;
				    		$prev=$line;
				    	}

				    	if(preg_match('/^(.*?)\t.*(Location)/', $line)){
				    		$location=preg_replace('/^(.*?)\t.*(Location)/', '${1}', $line);
				    		$found=true;
				    	}else{
				    		$prev=$line;
				    	}
				    	
				    	$i++;
				    }
				    fclose($handle);
		    	}
		    }


		    //Group similar patterns and count them all
		    $dynRegExp=[];
		    foreach ($places as $place) {
		    	if(array_key_exists(md5($place[0].$place[1]),$dynRegExp)){
		    		$dynRegExp[md5($place[0].$place[1])][1]++;
		    	}else{
		    		$dynRegExp[md5($place[0].$place[1])][0] = "/$place[0].*?\n.*?\n$place[1]/";
		    		$dynRegExp[md5($place[0].$place[1])][1]=1;
		    		$dynRegExp[md5($place[0].$place[1])][2]=$place[2];
		    	}
		    }
		    



		    uasort($dynRegExp,array($this,'orderRegExpHits'));
		    var_dump($dynRegExp);
		    

			print count($places);
			
		// }catch(\Exception $e){
		// 	echo "Error!!!!!!!!!!";
		// }
	}

	private function orderRegExpHits($a, $b){
		if ($a[1]>$b[1]) {
			return 1;
		}elseif($a[1]<$b[1]){
			return -1;
		}else{
			return 0;
		}
	}









	private function orderByProbability($a, $b){
		if ($a[3]>$b[3]) {
			return 1;
		}elseif($a[3]<$b[3]){
			return -1;
		}else{
			return 0;
		}
	}
}
