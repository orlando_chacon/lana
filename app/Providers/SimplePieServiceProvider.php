<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SimplePieServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('simplepie', function($app){
            return new SimplePie();
        });
    }
}
