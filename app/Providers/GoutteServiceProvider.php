<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Goutte\Client;

class Goutte extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('goutte', function($app){
            return new Client();
        });
    }
}
