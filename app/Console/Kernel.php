<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->call(function () {
            $start=microtime(true);
        
            $rsses=\App\Rss::all();
            $feedsUrls=[];
            foreach ($rsses as $rss) {
                // array_push($feedsUrls, $rss->url);
                $rss->fetch();
            }
            // $rss=new \App\Rss;

            $end=microtime(true);
            $exeTime = $end - $start;
            echo "Execution Time: $exeTime";
        })->everyFiveMinutes();

        $schedule->call(function () {
            $days = 1;

            DB::table('news')
                ->whereRaw('created_at < NOW() - INTERVAL ? DAY', [$days])
                ->delete();
            })->dailyAt('00:00');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
