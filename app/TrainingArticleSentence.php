<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingArticleSentence extends Model
{
    public function article()
	{
		return $this->belongsTo('App\TrainingArticle');
	}
	public function nGram($n)
	{
		$this->sentence = preg_replace('/\s+/', ' ', $this->sentence);

		$words = explode(' ', $this->sentence);

		for ($i=0; $i < count($words)-$n; $i++) { 
			for ($j=$i; $j < $i+$n; $j++) { 
				echo $words[$j];
			}
			echo "\n";
		}
		
	}


	
}
