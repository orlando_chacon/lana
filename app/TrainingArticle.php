<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingArticle extends Model
{
    public function language()
	{
		return $this->belongsTo('App\Language');
	}

	public function sentences()
	{
		return $this->hasMany('App\TrainingArticleSentence');
	}


	public function fetch($locations, $nArticles=10,$start=0){
		if (is_array($locations)) {
			foreach ($locations as &$city) {
				$city="\"$city\"";
				$city=str_replace(' ', '+', $city);
			}
			$query=implode("+and+", $locations);
		}
		$searchUrl = "https://www.google.com.ec/search?tbm=nws&q=" . $query . '&num='.$nArticles.'&start='.$start;
		echo "$searchUrl\n\n";
		$ch = curl_init();

	    // set url 
	    curl_setopt($ch, CURLOPT_URL, $searchUrl); 

	    //return the transfer as a string 
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

	    // $output contains the output string 
	    $output = curl_exec($ch);
	    // close curl resource to free up system resources 
	    curl_close($ch);

	    preg_match_all('/<\s*h3[^>]*>.*?href="\/url\?q=.*?(.*?)\&.*?<\s*\/h3\.*>/', $output, $newsUrls,PREG_OFFSET_CAPTURE);

		$curlMulti = curl_multi_init();
	    
	    $corpora="";
	    $chs=[];
	    $urls=[];
	    foreach ($newsUrls[1] as $id=>$newsUrl) {
	    	$chs[$id]=$ch = curl_init();
	    	curl_setopt($chs[$id], CURLOPT_URL, $newsUrl[0]); 

		    //return the transfer as a string 
		    curl_setopt($chs[$id], CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($chs[$id], CURLOPT_CONNECTTIMEOUT, 20); 
			curl_setopt($chs[$id], CURLOPT_TIMEOUT, 120);
		    curl_setopt($chs[$id], CURLOPT_FOLLOWLOCATION, true);
		    curl_setopt($chs[$id], CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

			curl_multi_add_handle($curlMulti,$ch);

			$urls[$id]=$newsUrl[0];
	    }

	  

		$running = null;
		do{
			curl_multi_exec($curlMulti, $running);
		}while($running > 0);
		$lang=new \App\Language;
		$htmlParser=new \App\HtmlParser;
		foreach($chs as $id => $ch) {
			$result[$id] = curl_multi_getcontent($ch);


			

		    $corpus=$htmlParser->clean($result[$id],true, true);

		    //Create and store into the DB the fetched training article
		    if ($corpus!='') {
		    	# code...
			    try {

			    	$detectedLanguage=$lang->detect($corpus);
					try {
				    	$training_article= new \App\TrainingArticle;
					    $training_article->url=$urls[$id];
					    $training_article->article=$corpus;
					    $training_article->query=$query;
					    $training_article->language_id=$detectedLanguage->id;
					    $training_article->save();
				    } catch (\Exception $e) {
				    	echo "***The article already exists in the DB\n";
				    }
			    } catch (\Exception $e) {
				    echo "***Error while detecting the article's language\n";
			    }
		    }
			curl_multi_remove_handle($curlMulti, $ch);
		}
		curl_multi_close($curlMulti);
	    echo $corpora;

	}

	public function splitInSentences()
	{
		$lang=new \App\Language;

		$sentences = $lang->sentenceSpliter($this->article);

		//Extract location from the query
		$query = str_replace('+', ' ', $this->query);
    	preg_match_all('/"(.*?)"/', $query, $locationsMatch,PREG_OFFSET_CAPTURE);
    	$locations=[];
    	foreach ($locationsMatch[1] as $location) {
			array_push($locations, $location[0]);
		}

		foreach ($sentences as $sentence) {
			$languageAndPunctuationRegexp=$lang->languageAndPunctuationRegexp($this->language->language);
			$sentence = str_replace(['.',',',':',';','!','?'], [' . ',' , ',' : ',' ; ',' ! ',' ? '], $sentence);
			$sentence = utf8_encode(trim(preg_replace([$languageAndPunctuationRegexp,'/\s+/'], ' ', $sentence)));
			$has_location=false;
			foreach ($locations as $location) {
				if(strrpos($sentence, $location) > 1){
					$has_location =true; 
					break;
				}
			}
			// echo $sentence. '<<<<->>>>>'. ($has_location ? 'Location detected: ' : 'NA: ').$this->language->language."<<<<\n\n";
			if($sentence!=''){
				$trainingArticleSentence = new \App\TrainingArticleSentence;
				$trainingArticleSentence->training_article_id = $this->id;
				$trainingArticleSentence->sentence = trim(str_replace($locations, '__location__',$sentence));
				$trainingArticleSentence->has_location = $has_location;
				$trainingArticleSentence->save();
			}
		}
	}
	public function buildNMGramModel($nLeft=2, $nRight=2){
		$language=\App\Language::find(1);
		$n=0;
		$surroundingWords=[];

		$surroundingMatrix=[];
		
		foreach ($language->trainingArticles as $article) {
			foreach ($article->sentences as $sentence) {
				$sentence=explode(' ', $sentence->sentence);
				for ($i=0; $i < count($sentence) ; $i++) {
					if ($sentence[$i]=='__location__') {
						$isLocation=1;
					}else{
						$isLocation=0;
					}

					$ngramPrev='';
					$ngramNext='';

					$ngramPrevWords=[];
					$ngramNextWords=[];
					
					for ($ri=-$nLeft; $ri <= $nRight; $ri++) { 
						if ($ri!=0 && array_key_exists($i+$ri, $sentence)) {
							// $surroundingWords[$ri]=$sentence[$i+$ri];
							if($ri<0){
								$ngramPrev.=$sentence[$i+$ri]. ' ';
								array_push($ngramPrevWords, $sentence[$i+$ri]);
							}else{
								$ngramNext.=$sentence[$i+$ri]. ' ';
							}
						}
					}
					$md5Index=md5($ngramPrev);
					if (!isset($surroundingMatrix[$md5Index])) {
						$surroundingMatrix[$md5Index][0]=$ngramPrev;
						$surroundingMatrix[$md5Index][1]=$ngramPrevWords;
						$surroundingMatrix[$md5Index][2]=0;
						$surroundingMatrix[$md5Index][3]=0;
					}
					if ($isLocation) {
						$surroundingMatrix[$md5Index][2]++;
					}else{
						$surroundingMatrix[$md5Index][3]++;
					}
				}
				// break;
			}
			// $n++;
			// if($n>=2){
			// 	break;
			// }
		}

		// for ($ri=-$nLeft; $ri <= $nRight; $ri++) {
		// 	if ($ri!=0){
		// 		asort($surroundingWords[$ri]);
		// 		asort($surroundingWordsFalse[$ri]);
		// 	}
		// }
		uasort($surroundingMatrix,array($this,'orderInverseBySubindex2'));

		print_r($surroundingMatrix);
		// for ($ri=-$nLeft; $ri <= $nRight; $ri++) {
		// 	if($ri!=0)
		// 	foreach ($surroundingWords[$ri] as $key => $value) {
		// 		echo $key."->".$value." : ";
		// 		echo isset($surroundingWordsFalse[$ri][$key]) ?$surroundingWordsFalse[$ri][$key]."\n": "\n";
		// 	}
		// }

		// print_r($surroundingWords);
		echo "end";
	}


	
	public function getSurroundingWords($nLeft=1, $nRight=0, $language="en"){
		$languages=\App\Language::all();
		foreach ($languages as $language) {
			foreach ($language->trainingArticles as $article) {
				foreach ($article->sentences as $sentence) {
					$sentence=explode(' ', $sentence->sentence);
					for ($i=0; $i < count($sentence) ; $i++) {
						if ($sentence[$i]=='__location__') {
							$isLocation=1;

							$ngram3Prev='';$ngram2Prev='';$ngram1Prev='';$ngram1Next='';$ngram2Next='';$ngram3Next='';

							$has3Prev=false;$has2Prev=false;$has1Prev=false;$has1Next=false;$has2Next=false;$has3Next=false;

							for ($ri=-$nLeft; $ri <= $nRight; $ri++) { 
								try {
									
								} catch (\Exception $e) {
									
								}
								if ($ri==-3 && array_key_exists($i+$ri, $sentence)) {
									$has3Prev=true;
								}elseif ($ri==-2 && array_key_exists($i+$ri, $sentence)) {
									$has2Prev=true;
								}elseif ($ri==-1 && array_key_exists($i+$ri, $sentence)) {
									$has1Prev=true;
								}elseif ($ri==0 && array_key_exists($i+$ri, $sentence)) {
									$has1Prev=true;
								}elseif ($ri==1 && array_key_exists($i+$ri, $sentence)) {
									$has3Next=true;
								}elseif ($ri==2 && array_key_exists($i+$ri, $sentence)) {
									$has2Next=true;
								}elseif ($ri==3 && array_key_exists($i+$ri, $sentence)) {
									$has1Next=true;
								}

								if ($ri!=0 && array_key_exists($i+$ri, $sentence)) {

									if($ri<0){
										if ($has3Prev) {
											$ngram3Prev.=$sentence[$i+$ri]. ' ';
										}
										if ($has2Prev) {
											$ngram2Prev.=$sentence[$i+$ri]. ' ';
										}
										if ($has1Prev) {
											if ($ri!=0) {
												$ngram1Prev.=$sentence[$i+$ri]. ' ';
											}
										}
									}else{
										if ($has1Next) {
											$ngram1Next.=$sentence[$i+$ri]. ' ';
										}
										if ($has2Next) {
											$ngram2Next.=$sentence[$i+$ri]. ' ';
										}
										if ($has3Next) {
											$ngram3Next.=$sentence[$i+$ri]. ' ';
										}
									}
								}
							}
							

							if ($has3Prev) {
								$article->createOrUpdateSurrounding(trim($ngram3Prev),-3);
							}
							if ($has2Prev) {
								$article->createOrUpdateSurrounding(trim($ngram2Prev),-2);
							}
							if ($has1Prev) {
								$article->createOrUpdateSurrounding(trim($ngram1Prev),-1);
							}
							if ($has1Next) {
								$article->createOrUpdateSurrounding(trim($ngram1Next),1);
							}
							if ($has2Next) {
								$article->createOrUpdateSurrounding(trim($ngram2Next),2);
							}
							if ($has3Next) {
								$article->createOrUpdateSurrounding(trim($ngram3Next),3);
							}

						}else{
							$isLocation=0;
						}
						
					}
				}
			}
		}

		
	}
	public function createOrUpdateSurrounding($string='',$n)
	{
		echo $this->url."\n<br>$string<br>\n";
		try {
			$search =	[' ',	'.'];
			$replace =	['\s',	'\.'];
			$regexpLeft=str_replace($search, $replace, $string);

			$surroundingWord=new \App\surroundingWord;
			$surroundingWord->string=$string;
			$surroundingWord->regexp='/\b'.$regexpLeft.'\s+(\p{Lu}.+)/su';
			$surroundingWord->count=1;
			$surroundingWord->language_id=$this->language_id;
			$surroundingWord->n=$n;
			$surroundingWord->save();
		} catch (\Exception $e) {
			$surroundingWord=\App\surroundingWord::where(['string'=>$string,'language_id'=>$this->language_id])->first();
			$surroundingWord->count++;
			$surroundingWord->save();
		}
	}




	private function orderInverseBySubindex2($a, $b){
		if($b[3]==0)$b[3]=0.01;
		if($a[3]==0)$a[3]=0.01;
		if ($a[2]/$a[3]>$b[2]/$b[3]) {
			return -1;
		}elseif($a[2]/$a[3]<$b[2]/$b[3]){
			return 1;
		}else{
			return 0;
		}
	}
}
