<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsRelevance extends Model
{
    public function news()
	{
		return $this->belongsTo('App\News');
	}
	public function geoname()
	{
		return $this->belongsTo('App\Geoname');
	}
}
