<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function stopWords()
	{
		return $this->hasMany('App\StopWord');
	}
	public function trainingArticles()
	{
		return $this->hasMany('App\TrainingArticle');
	}
	public function news()
	{
		return $this->hasMany('App\News');
	}
	public function surroundingWord()
	{
		return $this->hasMany('App\SurroundingWord');
	}

	public function detect($corpus)
	{
		$languages = \App\language::all();
		foreach ($languages as $language) {
			$languageRegexp=$this->getLanguageRexExp($language->language);

			$sWords = \App\StopWord::where('language_id',$language->id)->orderBy('language_id')->get(['word']);
			$regExpSearch=[];
			
			$corpusTmp=mb_strtolower(preg_replace([$languageRegexp,'/\s+/'], [' ',' '], $corpus));
	    	$tokens=explode(' ', $corpusTmp);
	    	$nTokens=count($tokens);
	    	$nMatches=0;
	    	// echo $corpusTmp."|Lang";
	    	// return false;
			foreach ($sWords as $sWord) {
		    	// preg_match_all($regExp[0], $corpus, $matches,PREG_OFFSET_CAPTURE);
	    		preg_match_all('/\b'.$sWord->word.'\b/', $corpusTmp, $matches,PREG_OFFSET_CAPTURE);
		    	$nMatches += count($matches[0]);
			}
			$langMatches[$language->language] = $nMatches/$nTokens;
		}
		uasort($langMatches,array($this,'orderInverse'));
		

		return \App\Language::where('language',key($langMatches))->first();

		
	}

	//Split corpus sentences according to a delimiter pattern.
	public function sentenceSpliter($corpus){
		$delimiter='/.*?(?<!\w\.\w.)(?<![A-Z][a-z]\.)((?<=\.|\?|!)\s|$)/';
		$corpus=preg_match_all($delimiter, $corpus, $matches);
		return $matches[0];
	}

	

	public function getLanguageRexExp($language){
		switch ($language) {
		    	case 'en':
		    		$languageRegexp = '/[^a-zA-Z\s]/'; 
		    		break;
		    	case 'es':
		    		$languageRegexp = '/[^a-zA-ZáéíóúÁÉÍÓÚñÑüÜ\s]/'; 
		    		break;
		    	case 'fr':
		    		$languageRegexp = '/[^a-zA-ZàâäôéèëêïîçùûüÿæœÀÂÄÔÉÈËÊÏÎŸÇÙÛÜÆŒ\s]/'; 
		    		break;
		    	case 'pt':
		    		$languageRegexp = '/[^a-zA-Z0-9µùàçéèçÇ\s]/'; 
		    		break;
		    	case 'de':
		    		$languageRegexp = '/[^a-zA-ZäöüßÄÖÜẞ\s]/'; 
		    		break;
		    	case 'it':
		    		$languageRegexp = '/[^a-zA-ZàèéìíîòóùúÀÈÉÌÍÎÒÓÙÚ\s]/'; 
		    		break;
		    	default:
		    		return false;
		    		break;
		    }
		return $languageRegexp;
	}
	public function languageAndPunctuationRegexp($language){
		switch ($language) {
		    	case 'en':
		    		$languageRegexp = '/[^a-zA-Z.,:;!?\s]/'; 
		    		break;
		    	case 'es':
		    		$languageRegexp = '/[^a-zA-ZáéíóúÁÉÍÓÚñÑüÜ.,:;!?\s]/'; 
		    		break;
		    	case 'fr':
		    		$languageRegexp = '/[^a-zA-ZàâäôéèëêïîçùûüÿæœÀÂÄÔÉÈËÊÏÎŸÇÙÛÜÆŒ.,:;!?\s]/'; 
		    		break;
		    	case 'pt':
		    		$languageRegexp = '/[^a-zA-Z0-9µùàçéèçÇ.,:;!?\s]/'; 
		    		break;
		    	case 'de':
		    		$languageRegexp = '/[^a-zA-ZäöüßÄÖÜẞ.,:;!?\s]/'; 
		    		break;
		    	case 'it':
		    		$languageRegexp = '/[^a-zA-ZàèéìíîòóùúÀÈÉÌÍÎÒÓÙÚ.,:;!?\s]/'; 
		    		break;
		    	default:
		    		return false;
		    		break;
		    }
		return $languageRegexp;
	}
	public function getLanguageRexExpLowerCase($language){
		switch ($language) {
		    	case 'en':
		    		$languageRegexp = '/[^a-z]+\b/'; 
		    		break;
		    	case 'es':
		    		$languageRegexp = '/[^a-záéíóúñü]+\b/'; 
		    		break;
		    	case 'fr':
		    		$languageRegexp = '/[^a-zàâäôéèëêïîçùûüÿæœ]+\b/'; 
		    		break;
		    	case 'pt':
		    		$languageRegexp = '/[^a-zµùàçéèç]+\b/'; 
		    		break;
		    	case 'de':
		    		$languageRegexp = '/[^a-zäöüß]+\b/'; 
		    		break;
		    	case 'it':
		    		$languageRegexp = '/[^a-zàèéìíîòóùú]+\b/'; 
		    		break;
		    	default:
		    		return false;
		    		break;
		    }
		return $languageRegexp;
	}
	private function orderInverse($a, $b){
		if ($a>$b) {
			return -1;
		}elseif($a<$b){
			return 1;
		}else{
			return 0;
		}
	}
	private function orderInverseBySubindex0($a, $b){
		if ($a[0]>$b[0]) {
			return -1;
		}elseif($a[0]<$b[0]){
			return 1;
		}else{
			return 0;
		}
	}
	

}
