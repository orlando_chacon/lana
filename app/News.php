<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
	public function language()
	{
		return $this->belongsTo('App\Language');
	}
	public function rss()
	{
		return $this->belongsTo('App\Rss');
	}
	public function relevances()
	{
		return $this->hasMany('App\NewsRelevance');
	}


	//This rules matches "Upper Upper UpperN lower Upper Upper UpperN lower" Allowing one lower between two Upper
	//$rule='/(\p{Lu}[\p{Lu}\p{Ll}]*\s*(\p{Ll}*){0,1}\s*)*(\p{Lu}[\p{Lu}\p{Ll}]*)+/us'

	//This rules matches "Upper Upper UpperN"
	//$rule='/(\p{Lu}[\p{Lu}\p{Ll}]*\s)*(\p{Lu}[\p{Lu}\p{Ll}]*)+/us'
	public function ruleParser($corpus=null, $rule='/(\p{Lu}[\p{Lu}\p{Ll}]*\s)*(\p{Lu}[\p{Lu}\p{Ll}]*)+/us'){
	    if(!isset($corpus)){
	    	$corpus=$this->corpus;
	    }

	    preg_match_all($rule, $corpus, $tokens);

	    $geoname = new \App\Geoname;
		foreach ($tokens[0] as $token) {
			echo ($token)." ---> ";
			$geonames = $geoname->getLocations($token);
			foreach ($geonames as $gn) {
				echo $gn->name2.", ";
			}
			echo "\n";
		}
		// print_r($tokens);
	}
    public function ngramParser($corpus, $n=3){
    	$lang = new \App\Language;
		$stopWord = new \App\StopWord;
		$geoname = new \App\Geoname;

		$sentences = $lang->sentenceSpliter($corpus);
		foreach ($sentences as &$sentence) {
			# code...
			$sentence=$stopWord->remove($sentence,true,true,true);
		}

		// dd($sentences);
		// print $corpus;
		// return false;



		$geonames = collect(new \App\Geoname);
		foreach ($sentences as $sentence) {
			$monoGrams=explode(' ', $sentence);
			$geonamesSentence = collect(new \App\Geoname);
			$nWords=count($monoGrams);
			for ($i=0; $i < $nWords; $i++) { 
				for ($m=$n; $m >=1 ; $m--) { 
					$query='';
					
					$containsLocation=false;
					
					if ($i+$m < $nWords) {
						for ($j=$i; $j < $i+$m; $j++) {
							$query.=$monoGrams[$j];
							$query.=$j < $i+$m-1 ? ' ' : '';
						}

						$query=trim($query);

						if($query!=''){
							$result = $geoname->getLocations($query);
							foreach ($result as $key => $value) {
								if($geonamesSentence->contains('id',$value->id)){
									unset($result[$key]);
								}
								$containsLocation=true;
								// echo $value->name2."\n";
							}
							$geonamesSentence = $geonamesSentence->merge($result);


							//Include a procedure to calc the relevance of individual words
							//Infrequent words must be preserved
							//The bigger the string query the bigger the relevance
							//The gazeteer contains some semiduplicate data, for instance, two entries with the same name and population, then only one must be preserved or two entries with the same name and coordinates, then only one must be preserved. or the three at the same time

							// foreach ($result as &$r) {
							// 	if(strlen($query) > strlen($r->name1)*0.6){
							// 		echo "\tname: ".$r->name1.':'. $r->population."<br>";
							// 	}else{
							// 		unset($r);
							// 	}
							// }
						}
					}
					if ($containsLocation) {
						break;
					}
					
				}
			}
			foreach ($geonamesSentence as $key => $value) {
				if($geonames->contains('id',$value->id)){
					unset($geonamesSentence[$key]);
				}
			}
			$geonames = $geonames->merge($geonamesSentence);
		}
		// foreach ($geonames as &$r) {
		// 		echo "\tname: ".$r->name1.':'. $r->population."<br>";
		// }		
		return $geonames;

	}

	public function detectLocations($value='')
	{
		$geonames=$this->ngramParser($this->title." ".$this->description);
		// $geonames=$this->ngramParser($this->corpus);
		// var_dump($geonames);
		echo "\n\n\n\n";
		foreach ( $geonames as $geoname) {
			echo $geoname->name2."\n";
			$newsRelevance = new \App\NewsRelevance;
			$newsRelevance->news_id=$this->id;
			$newsRelevance->lat=$geoname->lat;
			$newsRelevance->lng=$geoname->lng;
			$newsRelevance->geoname_id=$geoname->id;
			$newsRelevance->verticalExtent=1;
			$newsRelevance->horizontalExtent=1;
			$newsRelevance->save();
		}
	}
}
