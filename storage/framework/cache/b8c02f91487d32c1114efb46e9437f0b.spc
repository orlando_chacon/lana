a:4:{s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:1:{s:4:"feed";a:1:{i:0;a:6:{s:4:"data";s:24:"























";s:7:"attribs";a:1:{s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:5:"en-us";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:7:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:9:"Blog@Case";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"text";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:21:"http://blog.case.edu/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:2:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"rel";s:9:"alternate";s:4:"type";s:21:"application/xhtml+xml";s:4:"href";s:21:"http://blog.case.edu/";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}i:1;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:3:"rel";s:4:"self";s:4:"type";s:20:"application/atom+xml";s:4:"href";s:35:"http://blog.case.edu/news/feed.atom";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:6:"author";a:1:{i:0;a:6:{s:4:"data";s:4:"



";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:3:{s:4:"name";a:1:{i:0;a:5:{s:4:"data";s:5:"jms18";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:3:"uri";a:1:{i:0;a:5:{s:4:"data";s:27:"http://blog.case.edu/jms18/";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:5:"email";a:1:{i:0;a:5:{s:4:"data";s:21:"jeremy.smith@case.edu";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}}s:9:"generator";a:1:{i:0;a:5:{s:4:"data";s:12:"Movable Type";s:7:"attribs";a:1:{s:0:"";a:2:{s:3:"uri";s:36:"http://www.sixapart.com/movabletype/";s:7:"version";s:5:"3.121";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2006-07-21T17:40:40Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:5:"entry";a:15:{i:0;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:17:"Trackback is Back";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:227:"You may have noticed that the trackback functionality of the blog system has been disabled for some time now. We have been engaged in an epic struggle with spammers, and while most of the spammy trackbacks were caught by the...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:2024:"<p>You may have noticed that the <a title="CaseBlog/FAQ/Tech - CaseWiki" href="http://wiki.case.edu/CaseBlog/FAQ/Tech#What_is_Trackback">trackback</a> functionality of the blog system has been disabled for some time now.  We have been engaged in an epic struggle with spammers, and while most of the <em>spammy</em> trackbacks were caught by the system, the sheer number of machines attempting to spam us caused an effective <strong>Denial of Service</strong>, and we were forced to disable trackbacks to keep the blog system operational.</p>

<p>Well, we're happy to announce we've turned the tide on the spammers and are now blocking those computers that attempt to repeatedly hit the system with trackback or comment spam.  This should also reduce the amount of time you need to go in and spend effort marking icky comments and trackback as spam.</p>

<p>The new system has been running in a logging mode to gather metrics on what would be acceptable use versus spammer characteristics.  The heuristics used are fairly draconian.  A computer is permanently banned from reaching the blog system if they:<ul><li>Attempt to submit 5 comments in any 90 minute period and all of those comments end up labelled "moderated" by the other spam measures.</li><li>Attempt to submit 2 comments in any 4 hour period from an IP address whose earlier comments were marked as spam by a user of the blog system.</li><li>Attempt to trackback to an entry on the blog system 3 times in 90 minute period.</li><li>Attempt any trackback from an IP address whose earlier trackback(s) were marked as spam by a user of the blog system.</li></ul></p>

<p>If a computer does get blocked, they are given a message telling them so and asking them to email <a href="mailto:blog-admin@case.edu">blog-admin@case.edu</a> to get their computer unblocked.</p>

<p>As of right now, the new banning system has been running for under 24 hours and over 1000 IPs have been banned.</p>

<p>We're winning the war!  Go off and trackback to your heart's content!</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2006/07#009972";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2006/07#009972";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2006-07-21T17:37:19Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2006-07-21T17:40:40Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:1;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:43:"Yet Another Distributed Attack by a Spammer";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:242:"We had another spammer target the system; this time targeting comments. We've blocked the spammer and cleaned his comments from the database. If you receive email notifications of new comments on your weblog, you may have received a lot of...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:349:"<p>We had another spammer target the system; this time targeting comments.  We've blocked the spammer and cleaned his comments from the database.  If you receive email notifications of new comments on your weblog, you may have received <strong>a lot</strong> of emails over last night and this morning.  The offending comments have been deleted.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2006/04#008079";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2006/04#008079";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2006-04-08T19:55:07Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2006-04-08T19:54:16Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:2;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"Trackback Temporarily Disabled";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:198:"Trackback is temporarily disabled. A spammer has targetted us with a distributed attack. (We're currently having a little fun with him at his expense.) Trackback should be turned back on shortly....";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:330:"<p><a title="CaseBlog/FAQ/Tech - Edit this page - CaseWiki" href="http://wiki.case.edu/CaseBlog/FAQ/Tech#What_is_Trackback">Trackback</a> is temporarily disabled.  A spammer has targetted us with a distributed attack.  (We're currently having a little fun with him at his expense.)  Trackback should be turned back on shortly.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2006/04#008038";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2006/04#008038";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2006-04-07T04:31:47Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2006-04-07T04:34:07Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:3;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:11:"We're Back!";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:132:"And a little wary. Things might be a little rough around the edges as we finish getting the new software environment up to snuff....";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:137:"<p>And a little wary.  Things might be a little rough around the edges as we finish getting the new software environment up to snuff.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/11#004845";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/11#004845";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-11-18T04:20:53Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-11-18T04:21:46Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:4;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Intermittent Failures";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:264:"The server running the Case Blogging System (and it's sister site, the Case Wiki) is experiencing intermittent failures. The symptoms of the failures reveal themselves most prominently as the front page and Planet Case not refreshing and per-blog statistics not...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:970:"<p>The server running the <a title="Blog@Case" href="http://blog.case.edu/">Case Blogging System</a> (and it's sister site, the <a title="Main Page - CaseWiki" href="http://wiki.case.edu/Main_Page">Case Wiki</a>) is experiencing intermittent failures.  The symptoms of the failures reveal themselves most prominently as the front page and <a title="Planet Case" href="http://planet.case.edu/">Planet Case</a> not refreshing and per-blog statistics not getting updated.  (Other normal means of accessing the blog and the wiki services remain available, though.)</p>

<p>We are in the process of troubleshooting the root cause of the server's problem(s), but it seems that we may need to bring the server up and down repeatedly to find what exactly is going on.  In an effort to mitigate down time on the services, we are exploring our options to moving to a new server whilst we troubleshoot the current one.  Please expect some choppy waters as we weigh our options.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/11#004803";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/11#004803";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-11-15T19:31:59Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-11-15T19:32:49Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:5;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:19:"Requesting Feedback";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:233:"We've created an area on the Case Wiki to begin collecting ideas, requests, what-have-you on improvements to be made to the Case Blog system. Specifically, areas of the system that need improvements to help with the ease of use of...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:963:"<p>We've created an area on the <a title="Main Page - CaseWiki" href="http://wiki.case.edu/">Case Wiki</a> to begin collecting ideas, requests, what-have-you on improvements to be made to the <a title="Blog@Case" href="http://blog.case.edu/">Case Blog system</a>.  Specifically, areas of the system that need improvements to help with the <a title="Management Professor Notes II: using the Blog@Case platform" href="http://blog.case.edu/kep2/2005/10/10/using_the_blogcase_platform">ease of use</a> of the system.  So, if you have any ideas of feedback, head on over to <a title="CaseBlog:Suggestions - CaseWiki" href="http://wiki.case.edu/CaseBlog:Suggestions">http://wiki.case.edu/CaseBlog:Suggestions</a> and leave a note.  Alternatively, you can always <a href="mailto:blog-admin@case.edu?subject=The%20Case%20Blog%20System%20Is%20Nice%20but%20Could%20be%20Improved%20in%20One%20of%20the%20Following%20Ways">email us</a>.  We welcome all kinds of feedback.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/10#003924";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/10#003924";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-10-10T17:07:59Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-10-10T17:08:25Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:6;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:21:"Weblogs Being Deleted";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:204:"There is an error in the blog system that is causing weblogs to be deleted. As of rate now, we have had three user reports of this. Engineers are tracking down the problem, and we hope to have it fixed...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:459:"<p>There is an error in the blog system that is causing weblogs to be deleted.  As of rate now, we have had three user reports of this.  Engineers are tracking down the problem, and we hope to have it fixed shortly.</p>

<p>If this happens to you, please email the <a href="mailto:blog-admin@case.edu">Case Blog Administrators</a>, and we will attempt to restore your blog's content from backup.</p>

<p>Thank you, and we are sorry for this inconvenience.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/10#003576";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/10#003576";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-10-04T04:23:31Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-10-04T04:22:56Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:7;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:37:"Alumni Access to the Case Blog System";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:242:"Alumni have been granted access to the Case Blogging system! As of right now, this is on an experimental basis; so we can determine what load increases this incurs on the server, man-hours, and support. Disregarding all of that disclaimer,...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:298:"<p>Alumni have been granted access to the Case Blogging system!  As of right now, this is on an experimental basis; so we can determine what load increases this incurs on the server, man-hours, and support.</p>

<p>Disregarding all of that disclaimer, though... <strong>Welcome Alumni!</strong></p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/07#002404";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/07#002404";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-07-21T19:31:59Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-09-08T18:43:32Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:8;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:45:"Trackback is Back On... This Time, We Mean It";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:253:"Okay, [[Trackback]] is back on; and this time we're serious. We have (for the time being) conquered the spammers (die, spammers, die). If you notice a inordinate amount of [[Trackback]] spam on your blog, however, quickly email the administrators; so...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:519:"<p>Okay, <a href="http://wiki.case.edu/Trackback">Trackback</a> is back on; and this time we're <strong>serious</strong>.  We have (for the time being) conquered the spammers (<em>die, spammers, die</em>).</p>

<p>If you notice a inordinate amount of <a href="http://wiki.case.edu/Trackback">Trackback</a> spam on your blog, however, quickly email <a href="mailto:blog-admin@case.edu">the administrators</a>; so we can take care of the problem.</p>

<p>Thank you, all, for being patient with us during this problem.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/07#002299";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/07#002299";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-07-14T05:47:47Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-09-08T18:43:32Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:9;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:34:"Trackbacks (Once Again) Turned Off";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:102:"For those keeping score at home, [[Trackback]] has, once again, been turned off for the time being....";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:147:"<p>For those keeping score at home, <a href="http://wiki.case.edu/Trackback">Trackback</a> has, once again, been turned off for the time being.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/07#002292";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/07#002292";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-07-13T21:47:11Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-09-08T18:43:32Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:10;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:17:"Trackback Back On";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:243:"[[Trackback]] is back on... at least, temporarily while we monitor the situation. If you suddenly notice 10 or more Trackback spams all at once on your blog, please email blog-admin@case.edu to notify us of the problem. And, don't forget to...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:423:"<p><a href="http://wiki.case.edu/Trackback">Trackback</a> is back on... at least, temporarily while we monitor the situation.  If you suddenly notice 10 or more Trackback spams all at once on your blog, please email <a href="mailto:blog-admin@case.edu?subject=Argh!%20I%20Am%20Getting%20Spammed">blog-admin@case.edu</a> to notify us of the problem.  And, don't forget to despam your blog lest you end up all spammified.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/07#002281";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/07#002281";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-07-12T20:31:59Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-09-08T18:43:32Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:11;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:30:"Trackback Temporarily Disabled";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:220:"It seems the front layer defense against [[Trackback]] spam has decided to stop working. (There's a bug somewhere in the code that interacts with the Storable Perl module.) For the time being, trackbacks are disabled....";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:408:"<p>It seems the front layer defense against <a href="http://wiki.case.edu/Trackback">Trackback</a> spam has decided to stop working.  (There's a bug somewhere in the code that interacts with the <a title="search.cpan.org: Storable - persistence for Perl data structures" href="http://search.cpan.org/~ams/Storable-2.15/Storable.pm">Storable</a> Perl module.)  For the time being, trackbacks are disabled.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/07#002255";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/07#002255";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-07-10T21:31:31Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-09-08T18:43:32Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:12;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:41:"Problems Accessing the Blog Control Panel";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:230:"Over the night, some users may have experienced troubles logging in to the blog system. This was caused by an erroneous group in the directory server. Actually, it was caused by two erroneous groups; each claiming to be the one...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:604:"<p>Over the night, some users may have experienced troubles logging in to the blog system.  This was caused by an erroneous group in the directory server.  Actually, it was caused by <strong>two</strong> erroneous groups; each claiming to be the one true group that permits or denies access to the Weblog Control Panel.  We removed the more erroneous group and access returned to a status of unfettered.
</p>

<p>We now return you to your normal blogging ways.</p>

<p style="fontsize: 11px; color: #ccc">
This post was brought to you by the letter <strong>E</strong> for <strong>erroneous</strong>.
</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/07#002233";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/07#002233";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-07-08T17:59:47Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-09-08T18:43:32Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:13;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:6:"Doc Oc";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:214:"We received the news of Dr. Ocasio's passing with heavy hearts. To help memorialize his contributions to all of the lives he has touched, we have setup a blog where anyone that knew him can leave their memories....";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:408:"<p>We received the news of Dr. Ocasio's passing with heavy hearts.  To help memorialize his contributions to all of the lives he has touched, we have setup a <a title="In Memory of Dr. Ignacio Ocasio" href="http://blog.case.edu/dococ/">blog</a> where anyone that knew him can <a title="In Memory of Dr. Ignacio Ocasio: Doc Oc" href="http://blog.case.edu/dococ/2005/05/14/doc_oc">leave their memories</a>.</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/05#001692";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/05#001692";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-05-16T17:53:13Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-09-08T18:43:32Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}i:14;a:6:{s:4:"data";s:12:"











";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";s:5:"child";a:1:{s:27:"http://www.w3.org/2005/Atom";a:8:{s:5:"title";a:1:{i:0;a:5:{s:4:"data";s:40:"Call for Volunteers to Pilot Group Blogs";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"summary";a:1:{i:0;a:5:{s:4:"data";s:220:"Would you like your department to have a weblog? What about your organization? Or, even, one of your classes? You are in luck because we are looking for volunteers who would like to create group blogs. A group blog is...";s:7:"attribs";a:2:{s:0:"";a:1:{s:4:"type";s:4:"text";}s:36:"http://www.w3.org/XML/1998/namespace";a:1:{s:4:"lang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:2:"en";}}s:7:"content";a:1:{i:0;a:5:{s:4:"data";s:1228:"<p>Would you like your department to have a weblog?  What about your organization?  Or, even, one of your classes?</p>

<p>You are in luck because we are looking for volunteers who would like to create group blogs.  A group blog is one where there are multiple contributors to the same weblog.  A group blog can be used for announcements, a news site (like the <a title="Information Technology Services at Case" href="http://www.case.edu/its">ITS site</a>), or an enhanced discussion board meant to foster participation and communication.  Think <code>http://blog.case.edu/depts/mids</code> or <code>http://blog.case.edu/orgs/cheese_club</code> or <code>http://blog.case.edu/courses/eces131</code>; there are a lot of possibilities.</p>

<p><br />
If you are interested, or you think you may be interested you just aren't sure if you're interested and you would like to talk to someone who could get you interested, send an email to the <a href="mailto:blog-admin@case.edu?subject=About These Group Blogs...">Case Blog Administrators</a>.  We're happy to help you out in starting down this very interesting path of collaboration and improved channels of communication.</p>

<p>Happy Blogging!,<br />
Case Blog Administrators</p>";s:7:"attribs";a:1:{s:0:"";a:1:{s:4:"type";s:4:"html";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:8:"category";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:3:{s:4:"term";s:9:"/blognews";s:6:"scheme";s:26:"http://blog.case.edu/news/";s:5:"label";s:9:"blog-news";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:2:"id";a:1:{i:0;a:5:{s:4:"data";s:40:"http://blog.case.edu/news/2005/02#000664";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:4:"link";a:1:{i:0;a:5:{s:4:"data";s:0:"";s:7:"attribs";a:1:{s:0:"";a:4:{s:3:"rel";s:9:"alternate";s:4:"href";s:40:"http://blog.case.edu/news/2005/02#000664";s:4:"type";s:21:"application/xhtml+xml";s:8:"hreflang";s:2:"en";}}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:9:"published";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-02-11T20:31:11Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}s:7:"updated";a:1:{i:0;a:5:{s:4:"data";s:20:"2005-09-08T18:43:32Z";s:7:"attribs";a:0:{}s:8:"xml_base";s:0:"";s:17:"xml_base_explicit";b:0;s:8:"xml_lang";s:5:"en-us";}}}}}}}}}}}}s:4:"type";i:512;s:7:"headers";a:7:{s:4:"date";s:29:"Wed, 14 Feb 2018 21:15:19 GMT";s:6:"server";s:6:"Apache";s:13:"last-modified";s:29:"Thu, 06 Mar 2014 02:30:59 GMT";s:4:"etag";s:23:""2f27ce2-5524-ed78fec0"";s:13:"accept-ranges";s:5:"bytes";s:14:"content-length";s:5:"21796";s:12:"content-type";s:20:"application/atom+xml";}s:5:"build";s:14:"20180214170953";}