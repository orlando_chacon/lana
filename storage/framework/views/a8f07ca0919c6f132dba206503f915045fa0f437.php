<?php $__env->startSection('content'); ?>

<div id="portfolio">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">News <smaLL>that match your location preferences.</smaLL></h2>
			</div>
		</div>
		<div class="row">
			<?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="col-sm-4 row">
				<div class="col-sm-12 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-rss-square fa-3x"></i>
						</div>
					</div>
					<div class="container">
						<?php if($enclosure = $item->get_enclosure()): ?>
						<img class="img-fluid" src="<?php echo e($enclosure->get_link()); ?>" alt="">
                        <?php else: ?>
						<img class="img-fluid" src="/img/portfolio/01-thumbnail.jpg" alt="">
						<?php endif; ?>
					</div>
				</a>
				<div class="col-sm-12">
					<a href="<?php echo e(route('get_item').'?url='. urlencode($item->get_permalink())); ?>"><h5><?php echo e($item->get_title()); ?></h5></a>
					<h6>Detected Language: <span class="label label-success"><?php echo e($language->detect($item->get_title().' '.$item->get_description())); ?></span></h6>
					<p class="text-muted"><?php echo e($item->get_description()); ?></p>
					
					<h6>Locations detected: <span class="label label-success"><?php echo e($article->nGramParser($item->get_title().' '.$item->get_description(), 3)); ?></span></h6>
						<p><small>Posted on <?php echo e($item->get_date('j F Y | g:i a')); ?></small></p>
				</div>
				</div>

			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
	</div>
</div>
<?php echo $__env->make('common.modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>