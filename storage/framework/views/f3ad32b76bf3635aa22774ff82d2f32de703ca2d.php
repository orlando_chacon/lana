
 <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="item">
      <h2><a href="<?php echo e($item->get_permalink()); ?>"><?php echo e($item->get_title()); ?></a></h2>
      <p><?php echo e($item->get_description()); ?></p>
      <p><small>Posted on <?php echo e($item->get_date('j F Y | g:i a')); ?></small></p>

      
    </div>
   
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
