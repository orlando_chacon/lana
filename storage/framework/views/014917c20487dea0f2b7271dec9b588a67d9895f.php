<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>UPM | Location-Aware News Agregator</title>

    <?php echo $__env->make('common.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('css'); ?>
  </head>

  <body id="page-top">
    <?php echo $__env->make('common.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>



    <?php echo $__env->yieldContent('content'); ?>;
    
    <?php echo $__env->make('common.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 


    <?php echo $__env->make('common.foot', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
    <?php echo $__env->yieldContent('js'); ?>
  </body>

</html>
