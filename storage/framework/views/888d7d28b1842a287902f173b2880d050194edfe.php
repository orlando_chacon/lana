<?php $__env->startSection('content'); ?>

<div id="portfolio">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2 class="section-heading">News <smaLL>that match your location preferences.</smaLL></h2>
			</div>
		</div>
		<div class="row">
			<?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<div class="col-sm-4 row">
				<div class="col-sm-12 portfolio-item">
				<a class="portfolio-link" data-toggle="modal" href="#modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-rss-square fa-3x"></i>
						</div>
					</div>
					<div class="container">
						<img class="img-fluid" src="<?php echo e($article->image_url); ?>" alt="">
					</div>
				</a>
				<div class="col-sm-12">
					<a href="<?php echo e($article->url); ?>"><h5><?php echo e($article->title); ?></h5></a>
					<h6>Detected Language: <span class="label label-success"><?php echo e($article->language->language); ?></span></h6>
					<p class="text-muted"><?php echo e(substr($article->description,0,255)); ?> ...</p>
					
					<h6>Locations detected: <span class="label label-success">
						<?php $__currentLoopData = $article->relevances; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $location): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php echo e($location->geoname->name2); ?> <?php echo e($location->geoname->lat); ?>,<?php echo e($location->geoname->lng); ?><br>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</span></h6>
					<p><small>Posted on <?php echo e($article->post_date); ?></small></p>
				</div>
				</div>

			</div>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</div>
	</div>
</div>
<?php echo $__env->make('common.modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>